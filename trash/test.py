# -*- coding: utf-8 -*-

import logging
import _winreg, os, time, codecs

# 後々のことも考えてクラス化しておきます
class PSO2LogWatcher:
	logtypes = ["ChatLog", "ActionLog", "SymbolChatLog"]
	pso2_logdir = None

	def __init__(self):
		reg_folder = _winreg.OpenKey(
			_winreg.HKEY_CURRENT_USER,
			"Software\\Microsoft\\Windows\\CurrentVersion\\Explorer\\Shell Folders"
		)
		try:
			docdir = _winreg.QueryValueEx(reg_folder, "Personal")[0]
		except Exception as e:
			docdir = None
		finally:
			_winreg.CloseKey(reg_folder)
		assert docdir != None, u"ドキュメントの場所を特定できませんでした"
		self.__class__.pso2_logdir = docdir + "\\SEGA\\PHANTASYSTARONLINE2\\log"
		logging.info(u"PSO2のログのディレクトリ： {0}".format(self.pso2_logdir))

		self.logs = {}
		for logtype in self.logtypes:
			logfilename = self.find_logfile(logtype)
			logfileobj = codecs.open(logfilename, "r", "utf-16")
			self.logs[logtype] = {
				"type" : logtype,
				"logfilename" : logfilename,
				"logfileobj" : logfileobj,
				"logs" : [],
				"column" : len(logfileobj.readlines()) - 1
			}
			logging.info(u"{0}として{1}のファイルを使います。".format(logtype, logfilename))
			logging.debug(u"現在のカラム: {0}".format(self.logs[logtype]["column"]))

	# ログの種類からファイル名を返す
	def find_logfile(self, logtype):
		logfiles = os.listdir(self.pso2_logdir)
		logfile = filter(lambda fname: fname.startswith(logtype), logfiles)[-1]
		return self.pso2_logdir + "\\" + logfile

	def update_logs(self):
		for logtype in self.logtypes:
			self.update_log(logtype)

	def update_logfiles(self):
		for logtype in self.logtypes:
			watchlog = self.logs[logtype]
			newfilename = self.find_logfile(logtype)
			if watchlog["logfilename"] != newfilename:
				logging.info(u"{0}のファイルを{1}に変更します。".format(logtype, newfilename))
				watchlog["logfileobj"].close()
				watchlog["logfilename"] = newfilename
				watchlog["logfileobj"] = codecs.open(newfilename, "r", "utf-16")
				watchlog["column"] = 0
	
	def update_log(self, logtype):
		watchlog = self.logs[logtype]
		logfile = watchlog["logfileobj"]
		logfile.seek(0)
		loglines = logfile.readlines()
		maxidx = len(loglines) - 1
		if watchlog["column"] < maxidx:
			logging.debug(u"ログをカラム{0}からカラム{1}まで更新します。".format(watchlog["column"], maxidx))
			newlogs = self.separate_logs(loglines[watchlog["column"] + 1:])
			for logdic in newlogs:
				print logtype, logdic
			watchlog["logs"].append(newlogs)
			watchlog["column"] = maxidx

	def separate_logs(self, loglines):
		result = []
		for logline in loglines:
			parts = logline.strip().split("\t")
			logdic = {
				"date" : parts[0],
				"type" : parts[2],
				"character" : parts[4],
				"content" : parts[5]
			}
			result.append(logdic)
		return result

	def close_files(self):
		for logtype in self.logtypes:
			self.logs[logtype]["logfileobj"].close()

if __name__ == "__main__":
	logging.basicConfig(level=logging.DEBUG)
	watcher = PSO2LogWatcher()
	try:
		cnt = 0
		while True:
			watcher.update_logs()
			time.sleep(0.1)
			if cnt > 50:
				watcher.update_logfiles()
				cnt = 0
			cnt += 1
	except KeyboardInterrupt:
		logging.warning(u"終了します。")
	finally:
		watcher.close_files()
