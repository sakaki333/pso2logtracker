# -*- coding:utf-8 -*-

import wx, time

application = wx.App()
frame = wx.Frame(None, wx.ID_ANY, u"テストフレーム", size=(300,200))
 
panel = wx.Panel(frame, wx.ID_ANY)
panel.SetBackgroundColour("#AFAFAF")
 
element_array = ("element_1", "element_2", "element_4", "element_3", "element_5")
#listbox = wx.ListBox(panel, wx.ID_ANY, choices=element_array)
listbox = wx.ListBox(panel, wx.ID_ANY, choices=element_array, style=wx.LB_ALWAYS_SB)
 
layout = wx.GridSizer(1, 1)
layout.Add(listbox, flag=wx.GROW | wx.ALL, border=10)
 
panel.SetSizer(layout)
 
frame.Show()

while True:
	time.sleep(10)

application.MainLoop()
