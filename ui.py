# -*- coding: utf-8 -*-

import wx, wx.lib.mixins.listctrl as listmix
from collector import PSO2LogCollector

chattypes = [
    {
        "name" : u"周囲",
        "logtype" : "ChatLog",
        "columns" : [{"name" : u"キャラクター", "key" : "character"}, {"name" : u"チャット", "key" : "content"}],
        "value" : "PUBLIC"
    },
    {
        "name" : u"パーティー",
        "logtype" : "ChatLog",
        "columns" : [{"name" : u"キャラクター", "key" : "character"}, {"name" : u"チャット", "key" : "content"}],
        "value" : "PARTY"
    },
    {
        "name" : u"チーム",
        "logtype" : "ChatLog",
        "columns" : [{"name" : u"キャラクター", "key" : "character"}, {"name" : u"チャット", "key" : "content"}],
        "value" : "GUILD"
    },
    {
        "name" : u"ウィスパー",
        "logtype" : "ChatLog",
        "columns" : [{"name" : u"キャラクター", "key" : "character"}, {"name" : u"チャット", "key" : "content"}],
        "value" : "REPLY"
    },
    {
        "name" : u"拾得物",
        "logtype" : "ActionLog",
        "columns" : [{"name" : u"拾ったもの", "key" : "content"}, {"name" : u"備考", "key" : "content"}],
        "value" : "[Pickup]"
    }
]

class LoggerListCtrl(wx.ListCtrl, listmix.ListCtrlAutoWidthMixin):
    def __init__(self, parent, ID, colnum, pos=wx.DefaultPosition,
                 size=wx.DefaultSize, style=0):
        wx.ListCtrl.__init__(self, parent, ID, pos, size, style)
        listmix.ListCtrlAutoWidthMixin.__init__(self)
        self.setResizeColumn(colnum)

class LoggerFrame(wx.Frame):
    """
    #Class stuff decl
    """
    def __init__(self, chattype):
        wx.Frame.__init__(self, None, wx.ID_ANY, chattype["name"], size=(320,600))
        # TODO: まとめる
        self.logtype = chattype["logtype"]
        self.chattype = chattype["value"]
        self.columns = chattype["columns"]

        self.logcollector = PSO2LogCollector()
        self.logidx = -1
        self.cnt_reloadfile = 0

        self.timer=wx.Timer(self)

        self.Bind(wx.EVT_TIMER,self.evt_timer)
        self.Bind(wx.EVT_PAINT,self.paint)
        # self.Bind(wx.EVT_CLOSE, self.evt_close)

        panel = wx.Panel(self, wx.ID_ANY)
        panel.SetBackgroundColour("#AFAFAF")

        self.listctl = LoggerListCtrl(panel, wx.ID_ANY, len(self.columns), style=wx.LC_REPORT)
        for n, column in enumerate(self.columns):
            self.listctl.InsertColumn(n, column["name"], width = 100)
        #self.listctl.setResizeColumn(1)

        layout = wx.GridSizer(1, 1)
        layout.Add(self.listctl, flag=wx.GROW | wx.ALL, border=0)

        panel.SetSizer(layout)
        self.Show()

    def paint(self,event):
        self.timer.Start(100)# increase the value for more time

    def evt_timer(self,event):
        self.logcollector.update_logs()
        logs = self.logcollector.logs[self.logtype]["logs"]
        if (self.logidx <= len(logs) -1):
            for log in logs[self.logidx + 1:]:
                if log["type"] == self.chattype:
                    for n, column in enumerate(self.columns):
                        key = column["key"]
                        if n == 0:
                            self.listctl.InsertStringItem(0, log[key])
                        else:
                            self.listctl.SetStringItem(0, n, log[key])
                    #self.listbox.Insert(u"{0}    {1}".format(log["character"], log["content"]), 0)
            self.logidx = len(logs) - 1

        self.cnt_reloadfile += 1
        if self.cnt_reloadfile > 50:
            self.logcollector.update_logfiles()

if __name__ =="__main__":
    app = wx.App()
    frames = []
    for chattype in chattypes:
        frame = LoggerFrame(chattype)
        frames.append(frame)
    app.MainLoop()
