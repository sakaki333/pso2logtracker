# POS2LogTracker

PSO2のログを種別ごとに表示するやつです。

とりあえずNo Licenseとします。

## 使い方

wxPythonを入れてpython ui.pyを実行してください。

後々バイナリファイルも作ろうかと思っています。

## TODO

### UI

 * UIプロセスとコレクタープロセスの分離
 * チャットログの複数行表示
 * Winで実行可能なバイナリファイルの生成

### Collector

 * ログの種類ごとの処理の追加(メセタ額・武器の属性・ロビーアクションなど)

